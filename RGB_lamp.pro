#-------------------------------------------------
#
# Project created by QtCreator 2016-10-08T10:26:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = RGB_lamp
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../Qt/5.7/msvc2015_64/lib/ -lQt5SerialPort
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../Qt/5.7/msvc2015_64/lib/ -lQt5SerialPortd
else:unix: LIBS += -L$$PWD/../../../../Qt/5.7/msvc2015_64/lib/ -lQt5SerialPort

INCLUDEPATH += $$PWD/../../../../Qt/5.7/msvc2015_64/include
DEPENDPATH += $$PWD/../../../../Qt/5.7/msvc2015_64/include
