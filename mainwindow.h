#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_serialPort_pushButton_open_clicked();
    void on_serialPort_pushButton_close_clicked();
    void on_colour_horizontalSlider_B_valueChanged(int value);
    void on_colour_horizontalSlider_G_valueChanged(int value);
    void on_colour_horizontalSlider_R_valueChanged(int value);



private:
    Ui::MainWindow *ui;
    void setupMainApp(void);
    void mainApp(void);
    void setupSerialPort(void);
    void serialReadData(void);
    void colour_send();

    QSerialPort serialPort{};
    QByteArray serialPortBuffer{};
};

#endif // MAINWINDOW_H
