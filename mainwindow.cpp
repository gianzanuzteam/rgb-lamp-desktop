#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setupMainApp();
    mainApp();
}

MainWindow::~MainWindow()
{
    delete ui;
}

/* Setup inicial dos widgets */
void MainWindow::setupMainApp(void)
{
    /* Colour Sliders */
    ui->colour_horizontalSlider_R->setRange(0, 255);
    ui->colour_horizontalSlider_G->setRange(0, 255);
    ui->colour_horizontalSlider_B->setRange(0, 255);
    ui->colour_horizontalSlider_R->setValue(128);
    ui->colour_horizontalSlider_G->setValue(128);
    ui->colour_horizontalSlider_B->setValue(128);

    /* Colour display */
    ui->colour_lineEdit_R->setReadOnly(true);
    ui->colour_lineEdit_G->setReadOnly(true);
    ui->colour_lineEdit_B->setReadOnly(true);

    /* Widget display */
    ui->widget_R->setAutoFillBackground(true);
    ui->widget_G->setAutoFillBackground(true);
    ui->widget_B->setAutoFillBackground(true);
    ui->widget_RGB->setAutoFillBackground(true);

    /* Box de escolha de cor */
    ui->colour_box->setEnabled(false);

    /* Porta Serial */
    /* Evento: tratamento de bytes disponíveis na COM */
    connect(&serialPort, &QSerialPort::readyRead, this, &MainWindow::serialReadData);
    ui->serialPort_pushButton_open->setEnabled(true);
    ui->serialPort_pushButton_close->setEnabled(false);
    ui->serialPort_comboBox->setEnabled(true);
    setupSerialPort();
}

/* Setup inicial da porta serial */
void MainWindow::setupSerialPort(void)
{
    /* Listagem de COM na comboBox */
    ui->serialPort_comboBox->clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        ui->serialPort_comboBox->addItem(info.portName());
    }
}

/* APP principal */
void MainWindow::mainApp(void)
{
    /* Do nothing */
}

/* Evento: Mudança no valor do slider */
void MainWindow::on_colour_horizontalSlider_R_valueChanged(int value)
{
    ui->colour_lineEdit_R->setText(QString::number(value));
    ui->widget_R->setPalette(QPalette(QColor(value,0,0)));
    ui->widget_RGB->setPalette(QPalette(QColor(value,ui->colour_horizontalSlider_G->value(),ui->colour_horizontalSlider_B->value())));
    colour_send();
}
void MainWindow::on_colour_horizontalSlider_G_valueChanged(int value)
{
    ui->colour_lineEdit_G->setText(QString::number(value));
    ui->widget_G->setPalette(QPalette(QColor(0,value,0)));
    ui->widget_RGB->setPalette(QPalette(QColor(ui->colour_horizontalSlider_R->value(), value,ui->colour_horizontalSlider_B->value())));
    colour_send();
}
void MainWindow::on_colour_horizontalSlider_B_valueChanged(int value)
{
    ui->colour_lineEdit_B->setText(QString::number(value));
    ui->widget_B->setPalette(QPalette(QColor(0,0,value)));
    ui->widget_RGB->setPalette(QPalette(QColor(ui->colour_horizontalSlider_R->value(),ui->colour_horizontalSlider_G->value(),value)));
    colour_send();
}


/* EVENTO: Abrir serial Port */
void MainWindow::on_serialPort_pushButton_open_clicked()
{
    serialPort.setPortName(ui->serialPort_comboBox->currentText());

    /* Ajuste das configurações da COM */
    serialPort.setBaudRate(QSerialPort::Baud115200);
    serialPort.setDataBits(QSerialPort::Data8);
    serialPort.setParity(QSerialPort::NoParity);
    serialPort.setStopBits(QSerialPort::OneStop);
    serialPort.setFlowControl(QSerialPort::NoFlowControl);

    if(serialPort.open(QIODevice::ReadWrite))
    {
        ui->textBrowser->setText("<html><b>Serial</html></b>: Porta Serial-USB iniciada com sucesso.");
        ui->serialPort_pushButton_close->setEnabled(true);
        ui->serialPort_pushButton_open->setEnabled(false);
        ui->serialPort_comboBox->setEnabled(false);

        /* Box de escolha de cor */
        ui->colour_box->setEnabled(true);
    }
    else
    {
        ui->textBrowser->setText("<html><b>Serial</html></b>: Erro ao abrir porta Serial-USB.");
    }
}

/* EVENTO: Fechar serial Port */
void MainWindow::on_serialPort_pushButton_close_clicked()
{
    serialPort.clear();
    serialPort.close();

    ui->textBrowser->setText("<html><b>Serial</html></b>: Porta Serial-USB finalizada com sucesso.");

    /* widgets da porta serial */
    setupSerialPort();
    ui->serialPort_pushButton_open->setEnabled(true);
    ui->serialPort_pushButton_close->setEnabled(false);
    ui->serialPort_comboBox->setEnabled(true);

    /* Box de escolha de cor */
    ui->colour_box->setEnabled(false);
}

/* EVENTO: Leitura da porta COM */
/* Não utilizado - Sem ACK */
void MainWindow::serialReadData(void)
{
    serialPortBuffer.append(serialPort.readAll());
    serialPortBuffer.clear();
}

/* Envia configuração atual */
void MainWindow::colour_send()
{
    serialPort.write("#SET_COLOUR\n");
    serialPort.write(QString::number((ui->colour_lineEdit_R->text().toInt() -255)*(-1)).toLatin1() + "\n"); /* Maracutaia braba para inverter lógica */
    serialPort.write(QString::number((ui->colour_lineEdit_G->text().toInt() -255)*(-1)).toLatin1() + "\n");
    serialPort.write(QString::number((ui->colour_lineEdit_B->text().toInt() -255)*(-1)).toLatin1() + "\n");
    serialPort.write("#END\n");
}
